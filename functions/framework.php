<?php


/**
 * Trim data
 */
function safedata($value){
    $value = trim($value);
    return $value;
}


/**
 * function for handling GET|POST data
 */
function _global_data($request_name = 'get', $param, $defvalue = '') {
    switch($request_name){
        case 'post':
            $req = $_POST;
            break;
        
        case 'server':
            $req = $_SERVER;
            break;
        
        case 'get':
        default:
            $req = $_GET;
            break;
    }
    
    // check value
    if(!isset($req[$param])) 	{
        return $defvalue;
    }
    else {
        if(is_array($req[$param])){
            $result = array();
            foreach($req[$param] as $item){
                $result[] = safedata($item);
            }
            return $result;
        }
        else{
            return safedata($req[$param]);
        }
    }
}


/**
 * GET data
 */
function _get($param, $defvalue = ''){
    return _request_data('get', $param, $defvalue = '');
}


/**
 * POST data
 */
function _post($param, $defvalue = ''){
    return _request_data('post', $param, $defvalue = '');
}


/**
 * return data as json and set response code
 */
function jsonify($data, $code=200){
    header('Content-Type: application/json');
    http_response_code($code);
    
    if(!is_array($data)){
        $data = array($data);
    }
    
    echo json_encode($data);
    exit(0);
}


/**
 * return $routes item
 */
function route($id){
    global $routes;
    if(isset($routes[$id])){
        return $routes[$id];
    }
    else{
        return '';
    }
}


/**
 * function to get URL_PATH with high compatibility
 */
function get_url_path(){
    $currentURL = '';
    $currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    $currentURL .= $_SERVER["SERVER_NAME"];
 
    if($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443"){
        $currentURL .= ":".$_SERVER["SERVER_PORT"];
    } 
 
    $currentURL .= $_SERVER["REQUEST_URI"];
    $currentURL = parse_url($currentURL);
    if(isset($currentURL['path'])){
        $currentURL = $currentURL['path'];
    }
    else{
        $currentURL = '';
    }
    $currentURL = substr($currentURL, strpos($currentURL, APP_ROOT)+strlen(APP_ROOT));
    return $currentURL;
}