<?php

require_once 'config.php';

// load libs
$libs = glob('libs/*{.php}', GLOB_BRACE);
if(count($libs) != 0){
    foreach ($libs as $lib) {
        require_once($lib);
    }
}

// load functions
$functions = glob('functions/*{.php}', GLOB_BRACE);
if(count($functions) != 0){
    foreach ($functions as $function) {
        require_once($function);
    }
}

// configure ORM
ORM::configure(array(
    'connection_string' => 'mysql:host='.DB_HOST.';dbname='.DB_NAME,
    'username' => DB_USERNAME,
    'password' => DB_PASSWORD,
    'return_result_sets' => true,
    'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'),
    'logging' => true,
));


// framework router
$req = get_url_path();
$routes = explode('/', $req);
$handler = $routes['0'];
if(empty($handler)) {
    $handler = 'default';
}
$sys_render = __DIR__.'/controllers/' . $handler . '.php';
if (file_exists($sys_render)) {
    include($sys_render);
} else {
    jsonify(array(
        'error' => 1,
        'status_code' => 404,
        'msg'   => 'There is no controller with name ['.$routes[0].']',
    ), 404);
}

